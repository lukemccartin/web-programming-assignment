$(document).ready(function() {
    var total = [0,0,0,0,0,0,0,0];
    window.totalkesh = function(item, index){
        total[index] = item;
        var final = 0;
        for(var i =0; i<total.length; i++){
         	final = final + total[i]   
        }
		$("[name='price']").val(parseFloat(Math.round(final* 100) / 100).toFixed(2));
        document.getElementById('totalCost').innerHTML = ("$"+parseFloat(Math.round(final* 100) / 100).toFixed(2));
    };
	
	$("#SA").change(function(){		
		var find = document.getElementById("SA");
        var getqnumber = find.options[find.selectedIndex].value;
		var total;
		var price = document.getElementById("saPrice").innerHTML;
		if(!(getqnumber == 0))
		{
			total = getqnumber * price;
		}
		else
		{
			document.getElementById('saSubPrice').innerHTML = "$0.00";
       		total = 0;
		}
        document.getElementById('saSubPrice').innerHTML = ("$"+parseFloat(Math.round(total * 100) / 100).toFixed(2));
		totalkesh(total,1);
	});	
	
	$("#SP").change(function(){		
		var find = document.getElementById("SP");
        var getqnumber = find.options[find.selectedIndex].value;
		var total;
		var price = document.getElementById("spPrice").innerHTML;
		if(!(getqnumber == 0))
		{
			total = getqnumber * price;
		}
		else
		{
			document.getElementById('spSubPrice').innerHTML = "0.00";
			total = 0;
		}
		document.getElementById('spSubPrice').innerHTML = ("$"+parseFloat(Math.round(total * 100) / 100).toFixed(2));
        totalkesh(total,2);
	});	
	
	$("#SC").change(function(){		
		var find = document.getElementById("SC");
        var getqnumber = find.options[find.selectedIndex].value;
		var total;
		var price = document.getElementById("scPrice").innerHTML;
		if(!(getqnumber == 0))
		{
			total = getqnumber * price;
		}
		else
		{
			document.getElementById('scSubPrice').innerHTML = "0.00";
			total = 0;
		}
		document.getElementById('scSubPrice').innerHTML = ("$"+parseFloat(Math.round(total * 100) / 100).toFixed(2));
        totalkesh(total,3);
	});	
	
	$("#FA").change(function(){		
		var find = document.getElementById("FA");
        var getqnumber = find.options[find.selectedIndex].value;
		var total;
		var price = document.getElementById("faPrice").innerHTML;
		if(!(getqnumber == 0))
		{
			total = getqnumber * price;
		}
		else
		{
			document.getElementById('faSubPrice').innerHTML = "0.00";
			total = 0;
		}
		document.getElementById('faSubPrice').innerHTML = ("$"+parseFloat(Math.round(total * 100) / 100).toFixed(2));
       totalkesh(total,4);
	});	
		
	$("#FC").change(function(){		
		var find = document.getElementById("FC");
        var getqnumber = find.options[find.selectedIndex].value;
		var total;
		var price = document.getElementById("fcPrice").innerHTML;
		if(!(getqnumber == 0))
		{
			total = getqnumber * price;
		}
		else
		{
			document.getElementById('fcSubPrice').innerHTML = "0.00";
			total = 0;
		}
		document.getElementById('fcSubPrice').innerHTML = ("$"+parseFloat(Math.round(total * 100) / 100).toFixed(2));
        totalkesh(total,5);
       
	});	
		
	$("#B1").change(function(){		
		var find = document.getElementById("B1");
        var getqnumber = find.options[find.selectedIndex].value;
		var total;
		var price = document.getElementById("b1Price").innerHTML;
		if(!(getqnumber == 0))
		{
			total = getqnumber * price;
		}
		else
		{
			document.getElementById('b1SubPrice').innerHTML = "0.00";
			total = 0;
		}
		document.getElementById('b1SubPrice').innerHTML = ("$"+parseFloat(Math.round(total * 100) / 100).toFixed(2));
        totalkesh(total,6);
       
	});	

	$("#B2").change(function(){		
		var find = document.getElementById("B2");
        var getqnumber = find.options[find.selectedIndex].value;
		var total;
		var price = document.getElementById("b2Price").innerHTML;
		if(!(getqnumber == 0))
		{
			total = getqnumber * price;
		}
		else
		{
			document.getElementById('b2SubPrice').innerHTML = "0.00";
			total = 0;
		}
		document.getElementById('b2SubPrice').innerHTML = ("$"+parseFloat(Math.round(total * 100) / 100).toFixed(2));
       	totalkesh(total,7);
	});	
		
	$("#B3").change(function(){
		var find = document.getElementById("B3");
        var getqnumber = find.options[find.selectedIndex].value;
		var total;
		var price = document.getElementById("b3Price").innerHTML;
		if(!(getqnumber == 0))
		{
			total = getqnumber * price;
		}
		else
		{
			document.getElementById('b3SubPrice').innerHTML = "0.00";
			total = 0;
		}
		document.getElementById('b3SubPrice').innerHTML = ("$"+parseFloat(Math.round(total * 100) / 100).toFixed(2));
        totalkesh(total,8);
	});	
});