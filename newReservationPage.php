<?php
	require("header.php");
	$link1 = "movies.php";
	$link2 = "index.php";
	$link1Title = "Now Showing";
	$link2Title = "Home";
	include("nav.php");
?>
<script type="text/javascript" src="newResPageCalc.js"></script>
	<br>
	<div id="reserveWrapper">
	<?php
	try{
		if(!(isset($_POST['day']) && isset($_POST['time']) && isset($_POST['movie'])))
		throw new Exception ;
	} catch (Exception $e){
		header('Location: index.php');
		exit;
	}
	if(isset($_POST))
		{
			if($_POST['movie']== "CH")
			{
				echo "<h1>Inside Out</h1>";
				echo '<div class = "movieIMGDiv" id="mov1Poster">
				</div>';
				echo "<br>";
			}
			else if($_POST['movie']== "RC")
			{
				echo "<h1>Train Wreck</h1>";
				echo '<div class = "movieIMGDiv" id="mov2Poster">
				</div>';
				echo "<br>";
			}
			else if($_POST['movie']== "AC")
			{
				echo "<h1>Mission Impossible</h1>";
				echo "<div class = 'movieIMGDiv' id='mov3Poster'>
				</div>";
				echo "<br>";
			}
			else if($_POST['movie']== "AF")
			{
				echo "<h1>Girlhood</h1>";
				echo '<div class = "movieIMGDiv" id="mov4Poster">
				</div>';
				echo "<br>";
			}
			echo $_POST['day']; 
			echo " ";
			echo $_POST['time'];
			echo "<br>";
			
		}
		
		if($_POST['day']== "Monday" || $_POST['day']== "Tuesday" || ($_POST['time']=="1pm" && (!($_POST['day']=="Saturday" || $_POST['day']=="Sunday"))))
		{
			$SAPrice = 12.00;
			$SPPrice = 10.00;
			$SCPrice = 8.00;
			$FAPrice = 25.00;
			$FCPrice = 20.00;
			$B1Price = 20.00;
			$B2Price = 20.00;
			$B3Price = 20.00;
		}
		else
		{
			$SAPrice = 18.00;
			$SPPrice = 15.00;
			$SCPrice = 12.00;
			$FAPrice = 30.00;
			$FCPrice = 25.00;
			$B1Price = 30.00;
			$B2Price = 30.00;
			$B3Price = 30.00;
		}
	?>
	<br>
	<form method="post" action="myCart.php" id = "userDetailsReserveForm">
		<input type ="hidden" name="movie" value = "<?php echo $_POST['movie'];?>"/>
		<input type ="hidden" name="day" value = "<?php echo $_POST['day'];?>"/>
		<input type ="hidden" name="time" value = "<?php echo $_POST['time'];?>"/>
		<table>
			<thead>
				<tr>
					<th>Ticket Type</th>
					<th>Quantity</th>
					<th>Individual Price</th>
					<th>Subtotal Price</th>
				</tr>
			</thead>
			<tbody>
				<tr>
					<td class = "select">Adult</td>
					<td>
						<select id="SA" name ="SA">
							<option value = "0">0</option>    
							<option value = "1">1</option>
							<option value ="2">2</option>
							<option value ="3">3</option>
							<option value ="4">4</option>
							<option value ="5">5</option>
							<option value ="6">6</option>
							<option value ="7">7</option>
							<option value ="8">8</option>
							<option value ="9">9</option>
							<option value ="10">10</option>
						</select>    
					</td>
					<td>$<span name = "price" id= "saPrice"><?php echo $SAPrice ?></span></td>
					<td><span name = "subPrice" id="saSubPrice"></span></td>
				</tr>
				<tr>
					<td class = "select">Concession</td>
					<td>
						<select id="SP" name ="SP">
							<option value = "0">0</option>  
							<option value = "1">1</option>
							<option value ="2">2</option>
							<option value ="3">3</option>
							<option value ="4">4</option>
							<option value ="5">5</option>
							<option value ="6">6</option>
							<option value ="7">7</option>
							<option value ="8">8</option>
							<option value ="9">9</option>
							<option value ="10">10</option>
						</select>
					</td>
					<td>$<span name = "price" id="spPrice"><?php echo $SPPrice ?></span></td>
					<td><span name = "subPrice" id="spSubPrice"></span></td>
				</tr>
				<tr>
					<td class = "select">Child</td>
					<td>
						<select id="SC" name ="SC">
							<option value = "0">0</option>  
							<option value = "1">1</option>
							<option value ="2">2</option>
							<option value ="3">3</option>
							<option value ="4">4</option>
							<option value ="5">5</option>
							<option value ="6">6</option>
							<option value ="7">7</option>
							<option value ="8">8</option>
							<option value ="9">9</option>
							<option value ="10">10</option>
						</select>
					</td> 
					<td>$<span name = "price" id="scPrice"><?php echo $SCPrice ?></span></td>
					<td><span name = "subPrice" id="scSubPrice"></span></td>
				</tr>
				<tr>
					<td class = "select">First Class Adult</td>
					<td>
						<select id="FA" name ="FA">
							<option value = "0">0</option>      
							<option value = "1">1</option>
							<option value ="2">2</option>
							<option value ="3">3</option>
							<option value ="4">4</option>
							<option value ="5">5</option>
							<option value ="6">6</option>
							<option value ="7">7</option>
							<option value ="8">8</option>
							<option value ="9">9</option>
							<option value ="10">10</option>
						</select>
					</td>
					<td>$<span name = "price" id="faPrice"><?php echo $FAPrice ?></span></td>
					<td><span name = "subPrice" id="faSubPrice"></span></td>    
				</tr>
				<tr>
					<td class = "select">First Class Child</td>
					<td>
						<select id="FC" name ="FC">
							<option value = "0">0</option>      
							<option value = "1">1</option>
							<option value ="2">2</option>
							<option value ="3">3</option>
							<option value ="4">4</option>
							<option value ="5">5</option>
							<option value ="6">6</option>
							<option value ="7">7</option>
							<option value ="8">8</option>
							<option value ="9">9</option>
							<option value ="10">10</option>
						</select>
					</td> 
					<td>$<span name = "price" id="fcPrice"><?php echo $FCPrice ?></span></td>
					<td><span name = "subPrice" id="fcSubPrice"></span></td>    
				</tr>
				<tr>
					<td class = "select">Beanbag - 1 Person</td>
					<td>
						<select id="B1" name ="B1" >
							<option value = "0">0</option>      
							<option value = "1">1</option>
							<option value ="2">2</option>
							<option value ="3">3</option>
							<option value ="4">4</option>
							<option value ="5">5</option>
							<option value ="6">6</option>
							<option value ="7">7</option>
							<option value ="8">8</option>
							<option value ="9">9</option>
							<option value ="10">10</option>
						</select>
					</td>
					<td>$<span name = "price" id="b1Price"><?php echo $B1Price ?></span></td>  
					<td><span name = "subPrice" id="b1SubPrice"></span></td>    
				</tr>
				<tr>
					<td class = "select">Beanbag - 2 People</td>
					<td>
						<select id="B2" name ="B2">
							<option value = "0">0</option>      
							<option value = "1">1</option>
							<option value ="2">2</option>
							<option value ="3">3</option>
							<option value ="4">4</option>
							<option value ="5">5</option>
							<option value ="6">6</option>
							<option value ="7">7</option>
							<option value ="8">8</option>
							<option value ="9">9</option>
							<option value ="10">10</option>
						</select>
					</td> 
					<td>$<span name = "price" id="b2Price"><?php echo $B2Price ?></span></td> 
					<td><span name = "subPrice" id="b2SubPrice"></span></td>    
				</tr>
				<tr>
					<td class = "select">Beanbag - 3 Children</td>
					<td>
						<select id="B3" name ="B3">
							<option value = "0">0</option>      
							<option value = "1">1</option>
							<option value ="2">2</option>
							<option value ="3">3</option>
							<option value ="4">4</option>
							<option value ="5">5</option>
							<option value ="6">6</option>
							<option value ="7">7</option>
							<option value ="8">8</option>
							<option value ="9">9</option>
							<option value ="10">10</option>
						</select>
					</td>
					<td>$<span name = "price" id="b3Price"><?php echo $B3Price ?></span></td>
					<td><span name = "subPrice" id="b3SubPrice"></span></tr>    
				<tr>
					<td colspan ="3">Total Price</td>
					<td><span name = "price" id="totalCost" class="totalCosts"></span></td>
				</tr>
			</tbody>
		</table>
		<br>
		<input id="submitInput" type="submit" class="navButtonSmall" value ="Add to Cart" style="padding-bottom: 20px; padding-right: 80px" />
	</form>
	</div>
    <br>
	
<?php
	include("footer.php");	
?>
