<?php
	require("header.php");
	$link1 = "movies.php";
	$link2 = "contact.php";
	$link1Title = "Now Showing";
	$link2Title = "Contact Us";
	include("nav.php");
?>
	<br>
	<div id="wrapper">
		<div id="main">
			<h1>Grand Re-Opening Soon!</h1>
			<h3>Renovations in Progress Include:</h3>
			<ul>
				<li class="mainLI">Brand new seats including <strong>First Class section</strong> and <strong>Bean Bags</strong></li>
				<li class="mainLI">Fully equipped with state of the art <strong>3D</strong> projection capabilities</li> 
				<li class="mainLI">All new <strong>Dolby Surround Sound and Lighting</strong> systems installed</li>
			</ul>
			<img id="seatingIMG" src="SeatingLayout1.png" title="New Layout" alt="seatingimage"/>
		</div>
	
		<div id="sidebar">
			<h2>Discounted Ticket Prices!</h2>
			<p>
				Everybody loves cheap Tuesdays, so we decided to start a day earlier!
				At Silverado Cinemas Monday, Tuesday and 1pm every weekday is discounted so everyone 
				can afford to enjoy the cinema! For full movie schedule, click on the Now Showing button.
			</p>
			<p>
				Discounts up to $5 on all seats and $10 on beanbags! For a full list of ticket prices click below.
			</p>
			<div>
				<a href="#"><img src="ticket.png" id="ticketIMG" title="Click for prices" alt="ticketimage"/></a>
			</div>
			<div id="blackbg" style="display:none">
				<div id="ticketPop" class="lightbox" style="display:none">
					<div class="ticketSpace">
						<h2>Ticket Prices</h2>
						<table id="ticketPrices">
							<tr>
								<th></th>
								<th>
									Monday - Tuesday all day
									&amp; all 1pm weekday sessions!
								</th>
								<th>
									Wednesday to Friday regular prices
									&amp; Weekend sessions!
								</th>
							</tr>
							<tr>
								<td>Standard Full Price</td>
								<td>$12</td>
								<td>$18</td>
							</tr>
							<tr>
								<td>Standard Concession</td>
								<td>$10</td>
								<td>$15</td>
							</tr>
							<tr>
								<td>Standard Child Price</td>
								<td>$8</td>
								<td>$12</td>
							</tr>
							<tr>
								<td>First Class Adult</td>
								<td>$25</td>
								<td>$30</td>
							</tr>
							<tr>
								<td>First Class Child</td>
								<td>$20</td>
								<td>$25</td>
							</tr>
							<tr>
								<td>Beanbag*</td>
								<td>$20</td>
								<td>$30</td>
							</tr>
						</table>
						<p>
							*Beanbag price allows up to 2 adults OR 1 adult + 1 child OR up to 3 children.
						</p>
						<a href="movies.php" class="navButtonSmall">View Movies</a>
					</div>
				</div>
			</div>
		</div>
	</div>
	<br>
	<div id="socialNetworkLinks">
			<a href="http://facebook.com" class="facebookButton" title="like us!">facebook</a> | <a href="http://twitter.com" class="twitterButton" title="follow us!">twitter</a> | <a href="http://imdb.com" class="IMDbButton">IMDb</a>
	</div>

<?php
	include("footer.php");
?>