<?php
	require("header.php");
	$link1 = "movies.php";
	$link2 = "index.php";
	$link1Title = "Now Showing";
	$link2Title = "Home";
	include("nav.php");
?>
<br>
<div id="reserveWrapper">
	<h1>My Cart</h1>
	
	<?php
	
	if(isset($_POST['day']) && isset($_POST['time']) && isset($_POST['movie']))
	{	
		if($_POST['SA']==0)
		{
			unset($_POST['SA']);
		}
		if($_POST['SP']==0)
		{
			unset($_POST['SP']);
		}
		if($_POST['SC']==0)
		{
			unset($_POST['SC']);
		}
		if($_POST['FA']==0)
		{
			unset($_POST['FA']);
		}
		if($_POST['FC']==0)
		{
			unset($_POST['FC']);
		}
		if($_POST['B1']==0)
		{
			unset($_POST['B1']);
		}
		if($_POST['B2']==0)
		{
			unset($_POST['B2']);
		}
		if($_POST['B3']==0)
		{
			unset($_POST['B3']);
		}
		
		$_SESSION['cart']['screenings'][] = $_POST;
		header('Location: myCart.php', true, 303);
	}
	
	if(!(isset($_SESSION['cart']['screenings'])))
	{
		echo "<br><br><br><br><br><br><br><br><br>";
		echo "<h2>There is nothing in your Cart!</h2>";
		echo "<h2>You are being redirected to the Home page</h2>";
		echo "<br><br><br><br><br><br><br><br><br>";
		header( "Refresh:3; url=index.php", true, 303); 
	}
	else
	{
		for($x = 1; $x<count($_SESSION['cart']['screenings']); $x++)
		{
			for($y = 0; $y<count($_SESSION['cart']['screenings']); $y++)
			{
				if($_SESSION['cart']['screenings'][$x]['day'] == $_SESSION['cart']['screenings'][$y]['day'] && $_SESSION['cart']['screenings'][$x]['time'] ==  $_SESSION['cart']['screenings'][$y]['time']
					&& $_SESSION['cart']['screenings'][$x]['movie'] == $_SESSION['cart']['screenings'][$y]['movie'] && !($y == $x))
					{
						foreach($_SESSION['cart']['screenings'][$x] as $info => $value)
						{
							if($info == "SA" || $info == "SP" || $info == "SC" || $info == "FA" || $info == "FC" || $info == "B1" || $info == "B2" || $info == "B3")
							{
								$_SESSION['cart']['screenings'][$y][$info] += $_SESSION['cart']['screenings'][$x][$info];
								
								if($_SESSION['cart']['screenings'][$y][$info] > 10)
								{
									$_SESSION['cart']['screenings'][$y][$info] = 10;
								}
							}
						}
						unset($_SESSION['cart']['screenings'][$x]);
						$_SESSION ['cart']['screenings']= array_values($_SESSION["cart"]['screenings']);
						break;
					}
			}	
		}
		
		$grandTotal = 0;
		
		for($i = 0; $i<count($_SESSION['cart']['screenings']); $i++)
		{
			echo "<div class=booking id=$i>";
			echo "<h3>Booking "; echo $i+1; echo "</h3>"; 
			$screeningTotal = 0;
			$subtotal = 0;
			
			foreach($_SESSION['cart']['screenings'][$i] as $info => $value)
			{
				if($_SESSION['cart']['screenings'][$i]['day']== "Monday" || $_SESSION['cart']['screenings'][$i]['day']== "Tuesday" 
				|| ($_SESSION['cart']['screenings'][$i]['time']=="1pm" && (!($_SESSION['cart']['screenings'][$i]['day']=="Saturday" 
				|| $_SESSION['cart']['screenings'][$i]['day']=="Sunday"))))
				{
					$SAPrice = 12.00;
					$SPPrice = 10.00;
					$SCPrice = 8.00;
					$FAPrice = 25.00;
					$FCPrice = 20.00;
					$B1Price = 20.00;
					$B2Price = 20.00;
					$B3Price = 20.00;
				}
				else
				{
					$SAPrice = 18.00;
					$SPPrice = 15.00;
					$SCPrice = 12.00;
					$FAPrice = 30.00;
					$FCPrice = 25.00;
					$B1Price = 30.00;
					$B2Price = 30.00;
					$B3Price = 30.00;
				}
				
				if($value == "CH")
				{
					echo "<p>Inside Out</p>";
				}
				else if($value == "RC")
				{
					echo "<p>Train Wreck</p>";
				}
				else if($value == "AC")
				{
					echo "<p>Mission Impossible</p>";
				}
				else if($value == "AF")
				{
					echo "<p>Girlhood</p>";
				}
				else
					
				if($info == "day")
				{
					echo "<p>Showing on $value";
				}
				
				if($info == "time")
				{
					echo " at $value</p>";
				}

				if($info == "SA")
				{
					$subTotal = $value * $SAPrice;
					echo "<form method='post' class='cartForm'><select class='cartEdit' name = 'SA'>";
					echo "<option value = 0"; if($value == 0) echo " selected>"; else echo ">"; echo "0</option>";
					echo "<option value = 1"; if($value == 1) echo " selected>"; else echo ">"; echo "1</option>";
					echo "<option value = 2"; if($value == 2) echo " selected>"; else echo ">"; echo "2</option>";
					echo "<option value = 3"; if($value == 3) echo " selected>"; else echo ">"; echo "3</option>";
					echo "<option value = 4"; if($value == 4) echo " selected>"; else echo ">"; echo "4</option>";
					echo "<option value = 5"; if($value == 5) echo " selected>"; else echo ">"; echo "5</option>";
					echo "<option value = 6"; if($value == 6) echo " selected>"; else echo ">"; echo "6</option>";
					echo "<option value = 7"; if($value == 7) echo " selected>"; else echo ">"; echo "7</option>";
					echo "<option value = 8"; if($value == 8) echo " selected>"; else echo ">"; echo "8</option>";
					echo "<option value = 9"; if($value == 9) echo " selected>"; else echo ">"; echo "9</option>";
					echo "<option value = 10"; if($value == 10) echo " selected>"; else echo ">"; echo "10</option>";
					echo "</select>";
					echo " X ". "Standard Adult: $". $subTotal;
					echo "</form>";
					$screeningTotal += $subTotal;
				}
				else if($info == "SP")
				{
					$subTotal = $value * $SPPrice;
					echo "<form method='post' class='cartForm'><select class='cartEdit' name = 'SP'>";
					echo "<option value = 0"; if($value == 0) echo " selected>"; else echo ">"; echo "0</option>";
					echo "<option value = 1"; if($value == 1) echo " selected>"; else echo ">"; echo "1</option>";
					echo "<option value = 2"; if($value == 2) echo " selected>"; else echo ">"; echo "2</option>";
					echo "<option value = 3"; if($value == 3) echo " selected>"; else echo ">"; echo "3</option>";
					echo "<option value = 4"; if($value == 4) echo " selected>"; else echo ">"; echo "4</option>";
					echo "<option value = 5"; if($value == 5) echo " selected>"; else echo ">"; echo "5</option>";
					echo "<option value = 6"; if($value == 6) echo " selected>"; else echo ">"; echo "6</option>";
					echo "<option value = 7"; if($value == 7) echo " selected>"; else echo ">"; echo "7</option>";
					echo "<option value = 8"; if($value == 8) echo " selected>"; else echo ">"; echo "8</option>";
					echo "<option value = 9"; if($value == 9) echo " selected>"; else echo ">"; echo "9</option>";
					echo "<option value = 10"; if($value == 10) echo " selected>"; else echo ">"; echo "10</option>";
					echo "</select>";
					echo " X ". "Standard Concession: $". $subTotal. "<br>";
					echo "</form>";
					$screeningTotal += $subTotal;
				}
				else if($info == "SC")
				{
					$subTotal = $value * $SCPrice;
					echo "<form method='post' class='cartForm'><select class='cartEdit' name = 'SC'>";
					echo "<option value = 0"; if($value == 0) echo " selected>"; else echo ">"; echo "0</option>";
					echo "<option value = 1"; if($value == 1) echo " selected>"; else echo ">"; echo "1</option>";
					echo "<option value = 2"; if($value == 2) echo " selected>"; else echo ">"; echo "2</option>";
					echo "<option value = 3"; if($value == 3) echo " selected>"; else echo ">"; echo "3</option>";
					echo "<option value = 4"; if($value == 4) echo " selected>"; else echo ">"; echo "4</option>";
					echo "<option value = 5"; if($value == 5) echo " selected>"; else echo ">"; echo "5</option>";
					echo "<option value = 6"; if($value == 6) echo " selected>"; else echo ">"; echo "6</option>";
					echo "<option value = 7"; if($value == 7) echo " selected>"; else echo ">"; echo "7</option>";
					echo "<option value = 8"; if($value == 8) echo " selected>"; else echo ">"; echo "8</option>";
					echo "<option value = 9"; if($value == 9) echo " selected>"; else echo ">"; echo "9</option>";
					echo "<option value = 10"; if($value == 10) echo " selected>"; else echo ">"; echo "10</option>";
					echo "</select>";
					echo " X ". "Standard Child: $". $subTotal. "<br>";
					echo "</form>";
					$screeningTotal += $subTotal;
				}
				else if($info == "FA")
				{
					$subTotal = $value * $FAPrice;
					echo "<form method='post' class='cartForm'><select class='cartEdit' name = 'FA'>";
					echo "<option value = 0"; if($value == 0) echo " selected>"; else echo ">"; echo "0</option>";
					echo "<option value = 1"; if($value == 1) echo " selected>"; else echo ">"; echo "1</option>";
					echo "<option value = 2"; if($value == 2) echo " selected>"; else echo ">"; echo "2</option>";
					echo "<option value = 3"; if($value == 3) echo " selected>"; else echo ">"; echo "3</option>";
					echo "<option value = 4"; if($value == 4) echo " selected>"; else echo ">"; echo "4</option>";
					echo "<option value = 5"; if($value == 5) echo " selected>"; else echo ">"; echo "5</option>";
					echo "<option value = 6"; if($value == 6) echo " selected>"; else echo ">"; echo "6</option>";
					echo "<option value = 7"; if($value == 7) echo " selected>"; else echo ">"; echo "7</option>";
					echo "<option value = 8"; if($value == 8) echo " selected>"; else echo ">"; echo "8</option>";
					echo "<option value = 9"; if($value == 9) echo " selected>"; else echo ">"; echo "9</option>";
					echo "<option value = 10"; if($value == 10) echo " selected>"; else echo ">"; echo "10</option>";
					echo "</select>";
					echo " X ". "First Class Adult: $". $subTotal. "<br>";
					echo "</form>";
					$screeningTotal += $subTotal;
				}
				else if($info == "FC")
				{
					$subTotal = $value * $FCPrice;
					echo "<form method='post' class='cartForm'><select class='cartEdit' name = 'FC'>";
					echo "<option value = 0"; if($value == 0) echo " selected>"; else echo ">"; echo "0</option>";
					echo "<option value = 1"; if($value == 1) echo " selected>"; else echo ">"; echo "1</option>";
					echo "<option value = 2"; if($value == 2) echo " selected>"; else echo ">"; echo "2</option>";
					echo "<option value = 3"; if($value == 3) echo " selected>"; else echo ">"; echo "3</option>";
					echo "<option value = 4"; if($value == 4) echo " selected>"; else echo ">"; echo "4</option>";
					echo "<option value = 5"; if($value == 5) echo " selected>"; else echo ">"; echo "5</option>";
					echo "<option value = 6"; if($value == 6) echo " selected>"; else echo ">"; echo "6</option>";
					echo "<option value = 7"; if($value == 7) echo " selected>"; else echo ">"; echo "7</option>";
					echo "<option value = 8"; if($value == 8) echo " selected>"; else echo ">"; echo "8</option>";
					echo "<option value = 9"; if($value == 9) echo " selected>"; else echo ">"; echo "9</option>";
					echo "<option value = 10"; if($value == 10) echo " selected>"; else echo ">"; echo "10</option>";
					echo "</select>";
					echo " X ". "First Class Child: $". $subTotal. "<br>";
					echo "</form>";
					$screeningTotal += $subTotal;
				}
				else if($info == "B1")
				{
					$subTotal = $value * $B1Price;
					echo "<form method='post' class='cartForm'><select class='cartEdit' name = 'B1'>";
					echo "<option value = 0"; if($value == 0) echo " selected>"; else echo ">"; echo "0</option>";
					echo "<option value = 1"; if($value == 1) echo " selected>"; else echo ">"; echo "1</option>";
					echo "<option value = 2"; if($value == 2) echo " selected>"; else echo ">"; echo "2</option>";
					echo "<option value = 3"; if($value == 3) echo " selected>"; else echo ">"; echo "3</option>";
					echo "<option value = 4"; if($value == 4) echo " selected>"; else echo ">"; echo "4</option>";
					echo "<option value = 5"; if($value == 5) echo " selected>"; else echo ">"; echo "5</option>";
					echo "<option value = 6"; if($value == 6) echo " selected>"; else echo ">"; echo "6</option>";
					echo "<option value = 7"; if($value == 7) echo " selected>"; else echo ">"; echo "7</option>";
					echo "<option value = 8"; if($value == 8) echo " selected>"; else echo ">"; echo "8</option>";
					echo "<option value = 9"; if($value == 9) echo " selected>"; else echo ">"; echo "9</option>";
					echo "<option value = 10"; if($value == 10) echo " selected>"; else echo ">"; echo "10</option>";
					echo "</select>";
					echo " X ". "Bean Bag X 1 Person: $". $subTotal. "<br>";
					echo "</form>";
					$screeningTotal += $subTotal;
				}
				else if($info == "B2")
				{
					$subTotal = $value * $B2Price;
					echo "<form method='post' class='cartForm'><select class='cartEdit' name = 'B2'>";
					echo "<option value = 0"; if($value == 0) echo " selected>"; else echo ">"; echo "0</option>";
					echo "<option value = 1"; if($value == 1) echo " selected>"; else echo ">"; echo "1</option>";
					echo "<option value = 2"; if($value == 2) echo " selected>"; else echo ">"; echo "2</option>";
					echo "<option value = 3"; if($value == 3) echo " selected>"; else echo ">"; echo "3</option>";
					echo "<option value = 4"; if($value == 4) echo " selected>"; else echo ">"; echo "4</option>";
					echo "<option value = 5"; if($value == 5) echo " selected>"; else echo ">"; echo "5</option>";
					echo "<option value = 6"; if($value == 6) echo " selected>"; else echo ">"; echo "6</option>";
					echo "<option value = 7"; if($value == 7) echo " selected>"; else echo ">"; echo "7</option>";
					echo "<option value = 8"; if($value == 8) echo " selected>"; else echo ">"; echo "8</option>";
					echo "<option value = 9"; if($value == 9) echo " selected>"; else echo ">"; echo "9</option>";
					echo "<option value = 10"; if($value == 10) echo " selected>"; else echo ">"; echo "10</option>";
					echo "</select>";
					echo " X ". "Bean Bag X 2 People: $". $subTotal. "<br>";
					echo "</form>";
					$screeningTotal += $subTotal;
				}
				else if($info == "B3")
				{
					$subTotal = $value * $B3Price;
					echo "<form method='post' class='cartForm'><select class='cartEdit' name = 'B3'>";
					echo "<option value = 0"; if($value == 0) echo " selected>"; else echo ">"; echo "0</option>";
					echo "<option value = 1"; if($value == 1) echo " selected>"; else echo ">"; echo "1</option>";
					echo "<option value = 2"; if($value == 2) echo " selected>"; else echo ">"; echo "2</option>";
					echo "<option value = 3"; if($value == 3) echo " selected>"; else echo ">"; echo "3</option>";
					echo "<option value = 4"; if($value == 4) echo " selected>"; else echo ">"; echo "4</option>";
					echo "<option value = 5"; if($value == 5) echo " selected>"; else echo ">"; echo "5</option>";
					echo "<option value = 6"; if($value == 6) echo " selected>"; else echo ">"; echo "6</option>";
					echo "<option value = 7"; if($value == 7) echo " selected>"; else echo ">"; echo "7</option>";
					echo "<option value = 8"; if($value == 8) echo " selected>"; else echo ">"; echo "8</option>";
					echo "<option value = 9"; if($value == 9) echo " selected>"; else echo ">"; echo "9</option>";
					echo "<option value = 10"; if($value == 10) echo " selected>"; else echo ">"; echo "10</option>";
					echo "</select>";
					echo " X ". "Bean Bag X 3 People: $". $subTotal. "<br>";
					echo "</form>";
					$screeningTotal += $subTotal;
				}	

			}
			echo "<p>Total Session Price: $". $screeningTotal. "</p>";
			$_SESSION['cart']['screenings'][$i]['subTotal'] = $screeningTotal;
			$grandTotal += $screeningTotal;
			echo "<button type=button id=remove class=removeButton>delete item</button>";
			echo "</div>";
			echo "<br>";
		}
		$_SESSION['cart']['grandTotal'] = $grandTotal;
		echo "<p id=grandTotal>Grand Total Price: $". $grandTotal. "</p><br>";
		
		
	}
	
	
	?>
	
	<a href="movies.php" class="navButton" <?php if(!(isset($_SESSION['cart']['screenings']))) echo "style= 'display: none'" ?>>Book More</a> 
	<a href="checkout.php" class="navButton" <?php if(!(isset($_SESSION['cart']['screenings']))) echo "style= 'display: none'" ?>>Finalize Bookings</a>
</div>

<?php
	include("footer.php");	
?>
