var populate = function(title, poster, rating, summary, movieNumber)
{
      var heading = $('<h1></h1>');
      $(heading).html(title);
      heading.prependTo('#movie'+movieNumber);
    
      var posterIMG = $('<img>');
      posterIMG.attr('id',"poster"+movieNumber);
      posterIMG.attr('alt', title+" Poster");
      posterIMG.attr('src', poster);
      posterIMG.appendTo('#mov'+movieNumber+'Poster');
      
      var ratingIMG = $('<img>');
      ratingIMG.attr('src', rating);
      ratingIMG.attr('alt', "ratingIMG");
      ratingIMG.appendTo('#movie'+movieNumber);
      
      var summaryP = $('<p></p>');
      $(summaryP).html(summary);
      summaryP.appendTo('#movie'+movieNumber);
      
      var moreInfo = $('<button></button>');
      moreInfo.attr('class', "navButtonSmall");
      moreInfo.attr('id', "infoButton"+movieNumber);
      $(moreInfo).html("More Info");
      $(moreInfo).appendTo('#movie'+movieNumber);
};

var moreInfopop = function (movieNumber, trailer, description, screenings, movAbbr)
{
    $('#infoButton'+movieNumber).on('click', function() {
      $("<div></div>",{
          class: "moviespace",
          id: "moreMovie"+movieNumber
      }).appendTo("#moviePop"+movieNumber);
	  
      var trailerClip = $('<video controls>');
      trailerClip.attr('src', trailer);
	  trailerClip.attr('class',"movieTrailer")
      trailerClip.appendTo('#moreMovie'+movieNumber);
	  
	  for(var i = 0; i<description.length; i++)
	  {
		  var p = $('<p></p>');
		  p.attr('class', "movieDescription");
		  $(p).html(description[i]);
		  $(p).appendTo('#moreMovie'+movieNumber);
	  }
	  
	  var session = $('<button></button>');
      session.attr('class', "navButtonSmall");
      session.attr('id', "sessionButton"+movieNumber);
      $(session).html("Session Times");
      $(session).appendTo('#moreMovie'+movieNumber);
	  
	  var sessionDiv = $('<div></div>');
	  sessionDiv.attr('class', "sessionDiv");
	  sessionDiv.attr('style', "display:none");
	  sessionDiv.attr('id', "movieSessions"+movieNumber);
	  $(sessionDiv).appendTo('#moreMovie'+movieNumber);  
	  
	  var selectHead = $('<h2>Click Session Time To Reserve A Ticket</h2>');
	  $(selectHead).appendTo("#movieSessions"+movieNumber);
	  
	  var sessionList = $('<ul></ul>');
	  sessionList.attr('class', "sessionList")
	  sessionList.attr('id', "sessionList"+movieNumber);
	  $(sessionList).appendTo('#movieSessions'+movieNumber);
	  
	  for(var j in screenings)
	  {
		var bookForm = $("<form action='newReservationPage.php' method='post'></form>");
		bookForm.attr('class', "bookingForm");
		bookForm.attr('id', "bookingForm"+ j + screenings[j] + movieNumber);
		$(bookForm).appendTo('#sessionList'+movieNumber);
	  
		var hiddenInput1 = $("<input type='hidden'>");
		hiddenInput1.attr('name',"day");
		hiddenInput1.attr('value',j);
		$(hiddenInput1).appendTo("#bookingForm"+ j + screenings[j] + movieNumber);
		var hiddenInput2 = $("<input type='hidden'>");
		hiddenInput2.attr('name',"time");
		hiddenInput2.attr('value',screenings[j]);
		$(hiddenInput2).appendTo("#bookingForm"+ j + screenings[j] + movieNumber);
		var hiddenInput3 = $("<input type='hidden'>");
		hiddenInput3.attr('name',"movie");
		hiddenInput3.attr('value',movAbbr);
		$(hiddenInput3).appendTo("#bookingForm"+ j + screenings[j] + movieNumber);  		
		  
		var sessionItem = $('<li></li>');
		$(sessionItem).appendTo("#bookingForm"+ j + screenings[j] + movieNumber);
		
		var link = $("<a></a>");
		$(link).html(j +" "+ screenings[j]);
		link.attr('href', "javascript: submitform(" +"'"+ j + screenings[j] + movieNumber +"'"+ ")");
		$(link).appendTo(sessionItem);
	  }
    
	  $("#sessionButton"+movieNumber).click(function(){
        $("#movieSessions"+movieNumber).slideToggle();
	  });
 
      $("#blackbg, #moviePop"+movieNumber).fadeIn();
     
	  });
    	
	  $('#blackbg').on('click', function() {
      $(".moviespace").remove();
		  $("#moviePop+movieNumber, #blackbg").hide();
    });
}

function submitform(formID)
{
  document.forms["bookingForm"+formID].submit();
}


$(document).ready(function(){
  $.getJSON("https://<?php echo $_SERVER['SERVER_NAME']; ?>/~e54061/wp/moviesJSON.php", 
    function(data) {
    if(data.CH)
    {
      populate(data.CH.title, data.CH.poster, data.CH.rating, data.CH.summary, 1);
      moreInfopop(1, data.CH.trailer, data.CH.description, data.CH.screenings, "CH");
    }
    if(data.RC)
    {
      populate(data.RC.title, data.RC.poster, data.RC.rating, data.RC.summary, 2);
      moreInfopop(2, data.RC.trailer, data.RC.description, data.RC.screenings, "RC");
    }
    if(data.AC)
    {
      populate(data.AC.title, data.AC.poster, data.AC.rating, data.AC.summary, 3);
      moreInfopop(3, data.AC.trailer, data.AC.description, data.AC.screenings, "AC");
    }
    if(data.AF)
    {
      populate(data.AF.title, data.AF.poster, data.AF.rating, data.AF.summary, 4);
      moreInfopop(4, data.AF.trailer, data.AF.description, data.AF.screenings, "AF");
    }
  });
  

	$( '#ticketIMG').click(function() {
    	$("#blackbg, #ticketPop").show();
	});		
	$( '#blackbg, #exit').click(function() {
		$("#ticketPop, #blackbg").hide();
    });

});

$(document).ready(function(){
	$(".removeButton").on("click", function(){
		var id = $(this).closest("div").attr("id"); 
		$.post("removeCartItem.php", {id:id}, function(data, status){
			location.reload(true);
		});
		$(this.closest("div")).remove();
	});
	
	$(".cartEdit").on("change", function(){
		var movie = $(document.activeElement).closest("div").attr("id"); 
		var edit = $(document.activeElement).val();
		var parent = $(document.activeElement).closest("select").attr("name");
		$.post("editCartItem.php", {edit:edit, parent:parent, movie:movie}, function(data, status){
			location.reload(true);
		});
	});
  
  $(".removeDetails").on("click", function(){
		$.post("removeDetails.php", function(data, status){
			location.reload(true);
		});
	});
});
