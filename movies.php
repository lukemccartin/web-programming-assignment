<?php
	require("header.php");
	$link1 = "contact.php";
	$link2 = "index.php";
	$link1Title = "Contact Us";
	$link2Title = "Home";
	include("nav.php");
?>
	<div class="moviesDiv">
		<div id= "div1">
			<div class = "movieIMGDiv" id="mov1Poster">
			</div>
			<div class="movieSynopsis" id ="movie1">
			</div>
			<div class="lightbox" id="moviePop1" style="display:none;">
			</div>
		</div>	
		<div id= "div2" >
			<div class = "movieIMGDiv" id ="mov2Poster">
			</div>
			<div class="movieSynopsis" id ="movie2">
			</div>
			<div class="lightbox" id="moviePop2" style="display:none;">
			</div>
		</div>
		<div id="div3">
			<div class = "movieIMGDiv" id ="mov3Poster">
			</div>
			<div class="movieSynopsis" id ="movie3">
			</div>
			<div class="lightbox" id="moviePop3" style="display:none;">
			</div>
		</div>
		<div id="div4">
			<div class = "movieIMGDiv" id="mov4Poster">
			</div>
			<div class="movieSynopsis" id ="movie4">
			</div>
			<div class="lightbox" id="moviePop4" style="display:none;">
			</div>
		</div>
	<div id="blackbg" style="display:none;">
	</div>	
	</div>
<?php
	include("footer.php");	
?>
