<?php
	require("header.php");
	$link1 = "movies.php";
	$link2 = "index.php";
	$link1Title = "Now Showing";
	$link2Title = "Home";
	include("nav.php");
?>
    <div id="contactWrapper">
		<h1 id="contactHeading"><strong>Contact Us</strong></h1>
		<form action='https://titan.csit.rmit.edu.au/~e54061/wp/testcontact.php' method='post'>
			<input type="email" name="email" placeholder="your.email@here.com" autofocus required>
			<select name="subject">
				<option value="General Enquiry">General Enquiry</option>
				<option value="Group and Corporate Bookings">Group and Corporate Bookings</option>
				<option value="Suggestions & Complaints">Suggestions & Complaints</option>
				<option value="Other">Other</option>
			</select><br>
			<textarea name="message" rows="15" maxlength="500" cols="60" placeholder="Enter message here..." required></textarea><br>
			<input id="resetButton" type="reset" name="reset" value="Reset Form">
			<input id="submitButton" type="submit">
		</form>
	</div>
		
	<div id="mapDiv">
		<h1>Find us</h1>
		<!-- embedded map sourced from https://www.google.com.au/maps/@-29.0140163,134.7620614,15z -->
		<iframe id="map" src="https://www.google.com/maps/embed?pb=!1m18!1m12!1m3!1d3489.1053478158638!2d134.7543706!3d-29.01387089999998!2m3!1f0!2f0!3f0!3m2!1i1024!2i768!4f13.1!3m3!1m2!1s0x2accdf68f4e61cb3%3A0xca916ce03cbccfe!2sLOT+2508+Hutchison+St%2C+Coober+Pedy+SA+5723!5e0!3m2!1sen!2sau!4v1440739262724"></iframe>
	</div>

<?php
	include("footer.php");	
?>
