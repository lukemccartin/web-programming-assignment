<?php
	require("header.php");
	$link1 = "movies.php";
	$link2 = "index.php";
	$link1Title = "Now Showing";
	$link2Title = "Home";
	include("nav.php");
	
	if(!(isset($_SESSION['user']['name']) && isset($_SESSION['user']['phone']) && isset($_SESSION['user']['email'])) && (isset($_POST['name']) && isset($_POST['phone']) && isset($_POST['email'])))
	{
		$_SESSION['user']['name'] =$_POST['name'];
		$_SESSION['user']['phone'] =$_POST['phone'];
		$_SESSION['user']['email'] =$_POST['email'];
		unset($_POST['name']);
		unset($_POST['phone']);
		unset($_POST['email']);
	}

?>

<?php
if(isset($_SESSION['user']['name']) && isset($_SESSION['user']['phone']) && isset($_SESSION['user']['email']))
{
$file = fopen("ticket.txt", "a+");
for($i=0; $i<count($_SESSION['cart']['screenings']); $i++)
{
	fwrite($file,'name :' . $_SESSION['user']['name'] . "\n" . 'phone :' . $_SESSION['user']['phone'] . "\n" . 'email :' . $_SESSION['user']['email'] . "\n" . 'voucher :' . "\n" . 'movie :' . $_SESSION['cart']['screenings'][$i]['movie'] . "\n" . 'day :' . $_SESSION['cart']['screenings'][$i]['day'] . "\n" . 'time :' . $_SESSION['cart']['screenings'][$i]['time'] . "\n" . 'booking done!' . "\n");
	fclose($file);
}	
}
?>

<div id="reserveWrapper">
	<form method = "post">
		Add Voucher: <input id="voucherInput" type = "text" name="voucher" pattern ="[0-9]{5}-[0-9]{5}-[A-Z]{2}" title="Invalid voucher format." placeholder ="00000-00000-XX" <?php if (isset($_SESSION['cart']['voucher'])) echo "disabled"?> required/>
		<input id="submitInput" type="submit" class="navButtonSmall" value ="Apply" style="padding-bottom: 20px; padding-right: 20px" />
	</form>
	<form method="post">
		Name: <input id="nameInput" type="text" name="name" pattern="([a-zA-Z\p{P}]+[ ]?)+" title="Letters, spaces and/or punctuation only."placeholder="Full name" value= "<?php if (isset($_SESSION['user']['name'])){ echo $_SESSION['user']['name'];}?>" <?php if (isset($_SESSION['user']['name'])) echo "disabled"?> autofocus required/>
		Phone Number: <input id="phoneInput" type='text' name='phone' pattern="\({0,1}((0|\+61)(2|4|3|7|8)){0,1}\){0,1}(\ |-){0,1}[0-9]{2}(\ |-){0,1}[0-9]{2}(\ |-){0,1}[0-9]{1}(\ |-){0,1}[0-9]{3}" 
		title="Innapropriate phone number." placeholder='0400123456 / 0300123456' value="<?php if (isset($_SESSION['user']['phone'])){ echo $_SESSION['user']['phone']; }?>" <?php if (isset($_SESSION['user']['phone'])) echo "disabled"?> required/>
		Email: <input id="emailInput" type='email' name='email' placeholder='your@email.here' value="<?php if (isset($_SESSION['user']['email'])){ echo $_SESSION['user']['email']; }?>" <?php if (isset($_SESSION['user']['email'])) echo "disabled"?> required/>
		<input id="submitInput" type="submit" class="navButtonSmall" value ="Finalize" style="padding-bottom: 20px; padding-right: 60px" />
	</form>
	<button type='button' id='details' class='removeDetails'>delete details</button>
</div>

<?php
	include("footer.php");	
?>
