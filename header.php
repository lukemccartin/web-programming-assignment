<?php
	session_start();
?>
<!DOCTYPE html>
<html>
  <head>
    <meta charset="UTF-8">
	<meta name="viewport" content="width=device-width, initial-scale=1.0, minimum-scale=1, maximum-scale=1">
	<link href='https://fonts.googleapis.com/css?family=Federo' rel='stylesheet' type='text/css'> <!-- Obtained from https://www.google.com/fonts#QuickUsePlace:quickUse/Family:Federo -->
	<link href='https://fonts.googleapis.com/css?family=Slabo+27px' rel='stylesheet' type='text/css'> <!-- Obtained from https://www.google.com/fonts/specimen/Slabo+27px -->
    <title>Silverado Cinemas</title>
	<link rel="icon" href="rodeosilver640.png" type="image/gif" sizes="16x16"/>
	<link type="text/css" rel="stylesheet" href="stylesheet.css"/>
	<script src="https://ajax.googleapis.com/ajax/libs/jquery/2.1.3/jquery.min.js"></script>
	<script src="https://ajax.googleapis.com/ajax/libs/jqueryui/1.11.4/jquery-ui.min.js"></script>
	<script type="text/javascript" src="JSONmovies.php"></script>
  </head>

  <body>
	<a href="myCart.php" class="navButtonSmall" id="cartButton">My Cart</a>
    <header>
    </header>